import { Spell } from "../models/spell";
import { Injectable } from "@angular/core";

@Injectable()
export class SpellDataService {
  globalSpellList = [
    {
      name: "Antimagic Field",
      castingTime: "1 action",
      school: "Abjuration",
      level: "8th-Level",
      range: "Self (10-foot-radius sphere)",
      components: "V, S, M (a pinch o f powdered iron or iron filings)",
      duration: "Concentration, up to 1 hour",
      description: "A 10-foot-radius invisible sphere of antimagic surrounds you. This area is divorced from the magical energy that suffuses the multiverse. Within the sphere, spells can’t be cast, summoned creatures disappear, and even magic items become mundane. Until the spell ends, the sphere moves with you, centered on you. Spells and other magical effects, except those created by an artifact or a deity, are suppressed in the sphere and can’t protrude into it. A slot expended to cast a suppressed spell is consumed. While an effect is suppressed, it doesn’t function, but the time is spends suppressed counts against its duration.  Targeted Effects. Spells and other magical effects, such as magic missile and charm person, that target a creature or an object in the sphere have no effect on that target.  Areas of Magic. The area of another spell or magical effect, such as fireball, can’t extend into the sphere. If the sphere overlaps an area of magic, the part of the area that is covered by the sphere is suppressed. For example, the flames created by a wall of fire are suppressed within the sphere, creating a gap in the wall if the overlap is large enough. Spells. Any active spell or other magical effect on a creature or an object in the sphere is suppressed while the creature or object is in it.  Magic Items. The properties and powers of magic items are suppressed in the sphere. For example, a +1 longsword in the sphere functions as a nonmagical longsword.  A magic weapon’s properties and powers are suppressed if it is used against a target in the sphere or wielded by an attacker in the sphere. If a magic weapon or a piece of magic ammunition fully leaves the sphere (for example, if you fire a magic arrow or throw a magic spear at a target outside the sphere), the magic of the item ceases to be suppressed as soon as it exits.  Magical Travel. Teleportation and planar travel fail to work in the sphere, whether the sphere is the destination or the departure point for such magical travel. A portal to another location, world, or plane of existence, as well as an opening to an extradimensional space such as that created by the rope trick spell, temporarily closes while in the sphere.  Creatures and Objects. A creature or object summoned or created by magic temporarily winks out of existence in the sphere. Such a creature instantly reappears once the space the creature occupied is no longer within the sphere.  Dispel Magic. Spells and magical effects such as dispel magic have no effect on the sphere. Likewise, the spheres created by different antimagic field spells don’t nullify each other.",
    },
    {
      name: "Druidcraft",
      castingTime: "1 action",
      school: "Transmutation",
      level: "cantrip",
      range: "30 feet",
      components: "V, S",
      duration: "Instantaneous",
      description: "Whispering to the spirits of nature, you create one of the following effects within range: You create a tiny, harmless sensory effect that predicts what the weather will be at your location for the next 24 hours. The effect might manifest as a golden orb for clear skies, a cloud for rain, falling snowflakes for snow, and so on. This effect persists for 1 round. You instantly make a flower bloom, a seed pod open, or a leaf bud bloom. You create an instantaneous, harmless sensory effect, such as falling leaves, a puff of wind, the sound of a small animal, or the faint order of skunk. The effect must fit in a 5-foot cube. You instantly light or snuff out a candle, a torch, or a small campfire.",
    },
    {
      name: "Guidance",
      castingTime: "1 action",
      school: "Divination",
      level: "cantrip",
      range: "Touch",
      components: "V, S",
      duration: "Concentration, up to 1 minute",
      description: "You touch one willing creature. Once before the spell ends, the target can roll a d4 and add the number rolled to one ability check of its choice. It can roll the die before or after making the ability check. The spell then ends.",
    },
    {
      name: "Mending",
      school: "Transmutation",
      level: "cantrip",
      castingTime: "1 minute",
      range: "Touch",
      components: "V, S, M (two lodestones)",
      duration: "Instantaneous",
      description: "This spell repairs a single break or tear in an object you touch, such as a broken key, a torn cloak, or a leaking wineskin. As long as the break or tear is no longer than 1 foot in any dimension, you mend it, leaving no trace of the former damage. This spell can physically repair a magic item or construct, but the spell can’t restore magic to such an object.",
    },
    {
      name: "Poison Spray",
      school: "Conjuration",
      level: "cantrip",
      castingTime: "1 action",
      range: "10 feet",
      components: "V, S",
      duration: "Instantaneous",
      description: "You extend your hand toward a creature you can see within range and project a puff of noxious gas from your palm. The creature must succeed on a Constitution saving throw or take 1d12 poison damage. This spell’s damage increases by 1d12 when you reach 5th level (2d12), 11th level (3d12), and 17th level (4d12).",
    },
    {
      name: "Produce Flame",
      school: "Conjuration",
      level: "cantrip",
      castingTime: "1 action",
      range: "Self",
      components: "V, S",
      duration: "10 Minutes",
      description: "A flickering flame appears in your hand. The flame remains there for the duration and harms neither you nor your equipment. The flame sheds bright light in a 10-foot radius and dim light for an additional 10 feet. The spell ends if you dismiss it as an action or if you cast it again. You can also attack with the flame, although doing so ends the spell. When you cast this spell, or as an action on a later turn, you can hurl the flame at a creature within 30 feet of you. Make a ranged spell attack. On a hit, the target takes 1d8 fire damage. This spell’s damage increases by 1d8 when you reach 5th level (2d8), 11th level (3d8), and 17th level (4d8).",
    },
    {
      name: "Resistance",
      school: "Abjuration",
      level: "cantrip",
      castingTime: "1 action",
      range: "Touch",
      components: "V, S, M (a miniature cloak)",
      duration: "Concentration, up to 1 minute",
      description: "You touch one willing creature. Once before the spell ends, the target can roll a d4 and add the number rolled to one saving throw of its choice. It can roll the die before or after making the saving throw. The spell then ends.",
    },
    {
      name: "Shillelagh",
      school: "Transmutation",
      level: "cantrip",
      castingTime: "1 bonus action",
      range: "Touch",
      components: "V, S, M (mistletoe, a shamrock leaf, and a club or quarterstaff)",
      duration: "1 Minute",
      description: "The wood of a club or a quarterstaff you are holding is imbued with nature’s power. For the duration, you can use your spellcasting ability instead of Strength for the attack and damage rolls of melee attacks using that weapon, and the weapon’s damage die becomes a d8. The weapon also becomes magical, if it isn’t already. The spell ends if you cast it again or if you let go of the weapon",
    },
    {
      name: "Thorn Whip",
      school: "Transmutation",
      level: "cantrip",
      castingTime: "1 action",
      range: "30 feet",
      components: "V, S, M (the stem of a plant with thorns)",
      duration: "Instantaneous",
      description: "You create a long, vine-like whip covered in thorns that lashes out at your command toward a creature in range. Make a melee spell attack against the target. If the attack hits, the creature takes 1d6 piercing damage, and if the creature is Large or smaller, you pull the creature up to 10 feet closer to you.  This spell’s damage increases by 1d6 when you reach 5th level (2d6), 11th level (3d6), and 17th level (4d6).",
    },
  ];
}
