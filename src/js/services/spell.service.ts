import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Spell } from '../models/spell';
import { SpellDataService } from '../services/spell-data.service';

@Injectable()
export class SpellService {
    public spells: Spell[];

    constructor(
        private http: Http,
        private spellDataService: SpellDataService) {
        }

    getSpells() {
        return this.spellDataService.globalSpellList;
    }

    getSpell(name) {
        return this.spellDataService.globalSpellList.filter(spell => spell.name === name)[0];
    }

    ngOnInit() {
        this.getSpells();
    }


}
