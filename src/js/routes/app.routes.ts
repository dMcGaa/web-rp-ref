import { provideRouter, RouterConfig } from '@angular/router';

import { DashboardComponent } from '../components/dashboard.component';
import { SpellDetailComponent } from '../components/spell-detail.component';


export const routes: RouterConfig = [
  {
    component: DashboardComponent,
    path: '',
  },
  {
    component: SpellDetailComponent,
    path: 'detail/:name',
  },
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes),
];
