export class Spell {
    name: string;
    castingTime: string;
    school: string;
    level: string;
    range: string;
    components: string;
    duration: string;
    description: string;
}
