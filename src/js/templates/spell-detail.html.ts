export const htmlTemplate = `
  <div class="spell-detail" *ngIf="spell">
    <h2>{{spell.name}}</h2>
    <div><label>Level:        </label>{{spell.level}}</div>
    <div><label>School:       </label>{{spell.school}}</div>
    <div><label>Cast Time:    </label>{{spell.castingTime}}</div>
    <div><label>Range:        </label>{{spell.range}}</div>
    <div><label>Components:   </label>{{spell.components}}</div>
    <div><label>Duration:     </label>{{spell.duration}}</div>
    <div class="spellDescription">{{spell.description}}</div>
    <button (click)="goBack()">Back</button>
  </div>
`;
