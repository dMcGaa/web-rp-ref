export const htmlTemplate = `
    <h3>Spells</h3>
    <ul class="spell-list">
      <li class="spell-item" *ngFor="let spell of spells" (click)="gotoDetail(spell)">
          {{spell.name}}
      </li>
    </ul>
`;
