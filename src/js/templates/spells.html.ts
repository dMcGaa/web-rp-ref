export const htmlTemplate = `
    <h2>My Spelles</h2>
    <ul class="spells">
        <li *ngFor="let hero of spells"
            [class.selected]="spell === selectedSpell"
            (click)="onSelect(spell)">
            <span class="spell-element">
                <span class="badge">{{spell.id}}</span> {{spell.name}}
            </span>
            <button class="delete-button" (click)="delete(spell, $event)">Delete</button>
        </li>
    </ul>
    
    <button (click)="addSpell()">Add New Spell</button>
    <div *ngIf="addingSpell">
        <my-spell-detail (close)="close($event)"></my-spell-detail>
    </div>
    <div *ngIf="selectedSpell">
        <h2>
            {{selectedSpell.name | uppercase}} is my spell
        </h2>
        <button (click)="gotoDetail()">View Details</button>
    </div>
`;
