import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { SpellService } from '../services/spell.service';

@Component({
    directives: [ROUTER_DIRECTIVES],
    providers: [
        SpellService
    ],
    selector: 'my-app',
    styleUrls: ['dist/css/component/app.component.css'],
    template: `
        <h1>{{title}}</h1>
        <nav>
            <a [routerLink]="['']">Spells</a>
        </nav>
        <router-outlet></router-outlet>
    `,
})
export class AppComponent {
    title = 'Roleplaying Reference';
}
