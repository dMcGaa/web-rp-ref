import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Spell } from '../models/spell';
import { SpellService } from '../services/spell.service';
import { SpellDataService } from '../services/spell-data.service';
import { htmlTemplate } from '../templates/spell-detail.html';

@Component({
    selector: 'my-spell-detail',
    styleUrls: ['dist/css/component/spell-detail.component.css'],
    template: htmlTemplate,
})

export class SpellDetailComponent implements OnInit, OnDestroy {
    spell: Spell;
    error: any;
    private sub: any;

    constructor(
        private spellService: SpellService,
        private spellDataService: SpellDataService,
        private route: ActivatedRoute,
        private router: Router) {
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            let name = params['name'];
            this.spell = this.spellService.getSpell(name);
        })
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    goBack(){
        window.history.back();
    }

}
