import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Spell } from '../models/spell';
import { SpellService } from '../services/spell.service';
import { htmlTemplate } from '../templates/dashboard.html';

@Component({
    selector: 'my-dashboard',
    styleUrls: ['dist/css/component/dashboard.component.css'],
    template: htmlTemplate,
})

export class DashboardComponent implements OnInit{
    public spells: any;
    
    constructor(
        private router: Router,
        private spellService: SpellService) {
        this.spells = [];
    }

    getSpells() {
        this.spells = this.spellService.getSpells();
    }

    gotoDetail(spell: Spell) {
        let link = ['/detail', spell.name];
        this.router.navigate(link);
    }

    ngOnInit() {
        this.getSpells();
    }
}
